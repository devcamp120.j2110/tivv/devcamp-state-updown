import {Component} from 'react';

class Calculate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 0
        }
    }
    onBtnIncClick = () => {
        this.setState({
            value: this.state.value + 1
        })
    }
    onBtnDecClick = () => {
        if(this.state.value >0) {
            this.setState({
                value: this.state.value - 1
            })
        }
    }
    render() {
        return(
            <div className='row p-3'>
                <div className='col-2 text-center'>
                <button className='btn btn-success px-4 text-larger' onClick={this.onBtnIncClick}>+</button>
                </div>
                <div className='col-2 text-center'>
                    <p style={{border: "1px solid red", padding: "5px 0px"}}>Kết quả: {this.state.value} </p>
                </div>
                <div className='col-2 text-center'>
                <button className='btn btn-primary px-4 text-larger' onClick={this.onBtnDecClick}>-</button>
                </div>
            </div>
        )
    }
}
export default Calculate;