import 'bootstrap/dist/css/bootstrap.min.css'
import Calculate from './components/Calculate';

function App() {
  return (
    <div>
      <Calculate/>
    </div>
  );
}

export default App;
